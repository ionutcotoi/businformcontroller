#!/usr/bin/env python
from openpyxl import load_workbook
import sys

HEADLINE_LEN = 16
CONTROL_LEN = 14

_telegrams_h_template = """
#ifndef _TELEGRAMS_H_
#define _TELEGRAMS_H_

#include "ibis_telegram.h"
#include "Arduino.h"

const ibisTelegram telegrame[] PROGMEM {
%s
};

#endif

"""
telegram = ' {CODE}, "{LINE}", "{FRONT_L1}", "{FRONT_L2}", "{SIDE_L1}", "{SIDE_L2}", "{CONTROL}" '

telegrams = []

wb = load_workbook(filename=sys.argv[1])
ws = wb.active

this_telegram = telegram.format(CODE="0".rjust(2),
                                LINE="".zfill(3),
                                FRONT_L1="OpenDev".ljust(HEADLINE_LEN),
                                FRONT_L2="".ljust(HEADLINE_LEN),
                                SIDE_L1="OpenDev".ljust(HEADLINE_LEN),
                                SIDE_L2="".ljust(HEADLINE_LEN),
                                CONTROL="BI@MBI@M".ljust(CONTROL_LEN))
telegrams.append(this_telegram)

for row in ws.iter_rows(row_offset=1):
    front_l1 = None
    front_l2 = None
    side_l1 = ""
    side_l2 = ""
    control = ""
    code = row[0].value
    line = row[1].value

    try:
        front_l1 = row[2].value
    except Exception as e:
        pass
    if front_l1 is None:
        front_l1 = ""

    try:
        front_l2 = row[3].value
    except Exception as e:
        pass
    if front_l2 is None:
        front_l2 = ""

    try:
        side_l1 = row[4].value
    except Exception as e:
        pass

    if side_l1 is None:
        side_l1 = ""

    try:
        side_l2 = row[5].value
    except Exception as e:
        pass

    if side_l2 is None:
        side_l2 = ""

    try:
        control = row[6].value
    except Exception as e:
        pass

    if control is None:
        control = ""

    if line is None:
        line = '000'

    if code is not None:
        try:
            if len(front_l1) > HEADLINE_LEN or \
                            len(front_l2) > HEADLINE_LEN or \
                            len(side_l1) > HEADLINE_LEN or \
                            len(side_l2) > HEADLINE_LEN:
                print "String longer than HEADLINE_LEN, new len:", \
                    max(len(front_l1), len(front_l1), len(side_l1), len(side_l2))
        except Exception as e:
            print front_l1, front_l2, side_l1, side_l2
            print e
        this_telegram = telegram.format(CODE=str(code).rjust(2),
                                        LINE=str(line).zfill(3),
                                        FRONT_L1=front_l1.ljust(HEADLINE_LEN),
                                        FRONT_L2=front_l2.ljust(HEADLINE_LEN),
                                        SIDE_L1=side_l1.ljust(HEADLINE_LEN),
                                        SIDE_L2=side_l2.ljust(HEADLINE_LEN),
                                        CONTROL=control.ljust(CONTROL_LEN))
        telegrams.append(this_telegram)

telegram_buffer = ""
for i in range(0, len(telegrams)):
    telegram_buffer += "  {" + telegrams[i] + "}"
    if i < len(telegrams)-1:
        telegram_buffer += ",\n"

output = _telegrams_h_template % (telegram_buffer, )

with open("telegrams.h", "w") as out_file:
    out_file.write(output)
