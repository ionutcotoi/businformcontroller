// A - Sens
// B - Lumina
// C - Sus
// D - Jos
int GetNumber()
{
   int num = 0;
   char key = kpd.get_key();
   while(key != '#')
   {
      switch (key)
      {
         case '\0':
            break;

         case '0': case '1': case '2': case '3': case '4':
         case '5': case '6': case '7': case '8': case '9':
            num = num * 10 + (key - '0');
            lcd.clear();
            lcd.print("Cod traseu: ");
            lcd.print(num);
            break;

         case '*':
            num = 0;
            lcd.clear();
            lcd.print("Cod traseu: ");            
            break;
      }

      key = kpd.get_key();
   }

   return num;
}

