#ifndef _IBIS_TELEGRAM_H_
#define _IBIS_TELEGRAM_H_

#include "Arduino.h"

#define ROUTE_SHORT_NAME_LEN     3+1
#define HEADLINE_LEN             46+1
#define CONTROL_LEN              14+1
#define Z_LEN                    6+1
#define DELAY_BETWEEN_TELEGRAMS  25000

typedef struct _ibisTelegram {
  unsigned int t_code;
  char traseu_sn[ROUTE_SHORT_NAME_LEN];
  char traseu_front_1[HEADLINE_LEN];
  char traseu_front_2[HEADLINE_LEN];
  char traseu_side_1 [HEADLINE_LEN];
  char traseu_side_2 [HEADLINE_LEN];
  char traseu_control[CONTROL_LEN];
} ibisTelegram;

#include "telegrams.h"

#endif
