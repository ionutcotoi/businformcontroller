#include <EEPROM.h>
#include <LiquidCrystal_I2C.h>
#include <Wire.h>
#include <stdio.h>
#include <SPI.h>
#include <SD.h>

#include <inttypes.h>
#include <Wire.h>

class i2ckeypad {
public:
  i2ckeypad(int);
  i2ckeypad(int, int, int);
  char get_key();
  void init();
  
private:
  void pcf8574_write(int, int);
  int pcf8574_byte_read(int);
};


//extern "C" {
  //#include "WConstants.h"
//}


/*
 *  PIN MAPPING
 *
 *  Here you can change your wire mapping between your keypad and PCF8574
 *  Default mapping is for sparkfun 4x3 keypad
 */

#define COL0  0  // P2 of PCF8574, col0 is usually pin 3 of 4x3 keypads
#define COL1  1  // P0 of PCF8574, col1 is usually pin 1 of 4x3 keypads
#define COL2  2  // P4 of PCF8574, col2 is usually pin 5 of 4x3 keypads
#define COL3  3
#define ROW0  7  // P1 of PCF8574, row0 is usually pin 2 of 4x3 keypads
#define ROW1  6  // P6 of PCF8574, row1 is usually pin 7 of 4x3 keypads
#define ROW2  5  // P5 of PCF8574, row2 is usually pin 6 of 4x3 keypads
#define ROW3  4  // P3 of PCF8574, row3 is usually pin 4 of 4x3 keypads


/*
 *  KEYPAD KEY MAPPING
 *
 *  Default key mapping for 4x4 keypads, you can change it here if you have or
 *  like different keys
 */

const char keymap[4][5] =
{
  "123A",
  "456B",
  "789C",
  "*0#D"
};


/*
 *  VAR AND CONSTANTS DEFINITION. Don't change nothing here
 *
 */

// Default row and col pin counts
int num_rows = 4;
int num_cols = 3;

// PCF8574 i2c address
int pcf8574_i2c_addr;

// Current search row
static int row_select;

// Current data set in PCF8574
static int current_data;

// Hex byte statement for each port of PCF8574
const int hex_data[8] = {0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80};

// Hex data for each row of keypad in PCF8574
const int pcf8574_row_data[4] = 
{
  hex_data[ROW1] | hex_data[ROW2] | hex_data[ROW3] |
  hex_data[COL0] | hex_data[COL1] | hex_data[COL2] | hex_data[COL3],
  hex_data[ROW0] | hex_data[ROW2] | hex_data[ROW3] |
  hex_data[COL0] | hex_data[COL1] | hex_data[COL2] | hex_data[COL3],
  hex_data[ROW0] | hex_data[ROW1] | hex_data[ROW3] |
  hex_data[COL0] | hex_data[COL1] | hex_data[COL2] | hex_data[COL3],
  hex_data[ROW0] | hex_data[ROW1] | hex_data[ROW2] |
  hex_data[COL0] | hex_data[COL1] | hex_data[COL2] | hex_data[COL3],
};

// Hex data for each col of keypad in PCF8574
int col[4] = {hex_data[COL0], hex_data[COL1], hex_data[COL2], hex_data[COL3]};


/*
 *  CONSTRUCTORS
 */

i2ckeypad::i2ckeypad(int addr)
{
  pcf8574_i2c_addr = addr;
}

i2ckeypad::i2ckeypad(int addr, int r, int c)
{
  pcf8574_i2c_addr = addr;
  num_rows = r;
  num_cols = c;
}


/*
 *  PUBLIC METHODS
 */

void i2ckeypad::init()
{
  // All PCF8574 ports high
  pcf8574_write(pcf8574_i2c_addr, 0xff);

  // Start with the first row
  row_select = 0;
}

char i2ckeypad::get_key()
{
  static int temp_key;

  int tmp_data;
  int r;

  int key = '\0';

  // Search row low
  pcf8574_write(pcf8574_i2c_addr, pcf8574_row_data[row_select]);

  for(r=0;r<num_cols;r++) {
    // Read pcf8574 port data
    tmp_data = pcf8574_byte_read(pcf8574_i2c_addr);

    // XOR to compare obtained data and current data and know
    // if some column are low
    tmp_data ^= current_data;

    // Key pressed!
    if(col[r] == tmp_data) {
      temp_key = keymap[row_select][r];
      return '\0';
    }
  }

  // Key was pressed and then released
  if((key == '\0') && (temp_key != '\0'))    
  {
    key = temp_key;
    temp_key = '\0';
    return key;
  }

  // All PCF8574 ports high again
  pcf8574_write(pcf8574_i2c_addr, 0xff);

  // Next row
  row_select++;
  if(row_select == num_rows) {
    row_select = 0;
  }

  return key;
}

/*
 *  PRIVATE METHODS
 */

void i2ckeypad::pcf8574_write(int addr, int data)
{
  current_data = data;

  Wire.beginTransmission(addr);
  Wire.write(data);
  Wire.endTransmission();
}

int i2ckeypad::pcf8574_byte_read(int addr)
{
  Wire.requestFrom(addr, 1);

  return Wire.read();
}


LiquidCrystal lcd(0x20);

#define ROWS 4
#define COLS 4

// With A0, A1 and A2 of PCF8574 to ground I2C address is 0x20
#define PCF8574_ADDR_KEYPAD 0x21

i2ckeypad kpd = i2ckeypad(PCF8574_ADDR_KEYPAD, ROWS, COLS);
unsigned int cod_traseu = 0;

// Defines
#define CLEAR_LINE "                "

//Prototypes
void saveDataToEEPROM();

//valoare nivel intre 0 si 29 inclusiv
void nivelIluminare(int nivel) {
  //
}

//linie 0 sau 1, iar coloana intre 0 si 16
void mutaCursorLaPozitia(int linie, int coloana){
  lcd.setCursor(coloana, linie);
}

void cursorTipLinieVizibil() {
  lcd.cursor();
  lcd.noBlink();
}

void cursorTipBaraVizibil() {
  lcd.cursor();
  lcd.blink();
}

void cursorInvizibil() {
  lcd.noCursor();
  lcd.noBlink();
}

class MenuItem {
public:
    char *Label;
    unsigned char *Value;
    char *Unit;
    unsigned char minValue;
    unsigned char maxValue;
    MenuItem *up;
    MenuItem *down;
    unsigned char isFloatValue;
    float    FloatValue;
    unsigned char cursor;
    unsigned char cursorMax, cursorMin;
    
public:
    MenuItem(char *label, unsigned char *initialValue, char *unit, unsigned char min, unsigned char max, unsigned char isFloat = 0) {
        Value = initialValue;
        isFloatValue = isFloat;
        if(isFloat)
          FloatValue = *Value / 10.00;
        Label = label;
        Unit = unit;  
        cursor = strlen(Label) + 2 ;
        cursorMin = cursor;
        cursorMax = cursor;
        minValue = min;
        maxValue = max;
    }

    void goUp(int linie) {
        (*Value)++;
        if (*Value > maxValue) 
          *Value = maxValue;
        mutaCursorLaPozitia(linie, cursor);
        lcd.print(CLEAR_LINE);        
        mutaCursorLaPozitia(linie, cursor);
        if (isFloatValue) {
          FloatValue = *Value / 10.00;
            char tmp[10];
             int tmpi = (FloatValue - (int)FloatValue) * 10;
             sprintf(tmp, "%d.%d", (int)FloatValue, tmpi);
             lcd.print(tmp);
        }
        else
          lcd.print(*Value, DEC);
        lcd.print(Unit);
    };

    void goDown(int linie) {
        if (*Value == minValue)
          return;
        (*Value)--;
        mutaCursorLaPozitia(linie, cursor);
        lcd.print(CLEAR_LINE);
        mutaCursorLaPozitia(linie, cursor);
        if (isFloatValue) {
          FloatValue = *Value / 10.00;
             char tmp[10];
             int tmpi = (FloatValue - (int)FloatValue) * 10;
             sprintf(tmp, "%d.%d", (int)FloatValue, tmpi);
             lcd.print(tmp);;
        }
        else
          lcd.print(*Value, DEC);
        lcd.print(Unit);
    };

    void left() {
        cursor--;
        if(cursor < cursorMin)  cursor = cursorMax;
        mutaCursorLaPozitia(0, cursor);
    };

    void right(){
        cursor++;
        if(cursor > cursorMax)  cursor = cursorMin;
        mutaCursorLaPozitia(0, cursor);
    };
};

class Screen {
MenuItem *items;
unsigned char numItems;
unsigned char selectedItem;
unsigned char lineEditing;
unsigned char lineSelect;

public:
    Screen(MenuItem *arrayOfItems, unsigned char numberOf) {
        items = arrayOfItems;    
        numItems = numberOf;
        selectedItem = 0;
        lineEditing = 0;
        lineSelect = 0;
    };
    
    void display() {
        lcd.clear();
        for(char i = 0; i<numItems; i++) {
           mutaCursorLaPozitia(i,0);
           lcd.print(items[i].Label);
           lcd.print(": ");
           if(items[i].isFloatValue) {
             char tmp[10];
             items[i].FloatValue = *(items[i].Value) / 10.00;
             int tmpi = (items[i].FloatValue - (int)items[i].FloatValue) * 10;
             sprintf(tmp, "%d.%d", (int)items[i].FloatValue, tmpi);
             lcd.print(tmp);
           }
           else
             lcd.print(*(items[i].Value), DEC);
           lcd.print(items[i].Unit);
        }
    }
    
    void goUp() {
        if(!lineEditing) {
            selectedItem--;
            if(selectedItem > numItems)
                selectedItem = numItems - 1;
            mutaCursorLaPozitia(selectedItem, strlen(items[selectedItem].Label) + 2);
        } else {
            items[selectedItem].goUp(selectedItem);

        }
    };

    void goDown() {
        if(!lineEditing) {
            selectedItem++;
            if(selectedItem == numItems)
                selectedItem = 0;
            mutaCursorLaPozitia(selectedItem, strlen(items[selectedItem].Label) + 2);
        } else {
            items[selectedItem].goDown(selectedItem);

        }
    };

    void beginCursor() {
        mutaCursorLaPozitia(selectedItem, strlen(items[selectedItem].Label) + 2);
        cursorTipLinieVizibil();
        lineSelect = 1;
    };

    void endCursor() {
        cursorInvizibil();
        lineSelect = 0;
    };

    void left() {
        if (lineEditing)
            items[selectedItem].left();
    };

    void right() {
        if(lineEditing)
            items[selectedItem].right();
    };

    void select() {
        if(lineSelect) {
            if(lineEditing == 0) {
                lineEditing = 1;
                cursorTipBaraVizibil();
            } else {
                lineEditing = 0;
                cursorTipLinieVizibil();
                saveDataToEEPROM();
            }
        } 
    }

};


class LCDManager {
public:
    int numLines;
    int numColumns;

    Screen *screenArray;
    unsigned char numScreens;
    unsigned char currentScreen;
    unsigned char isInEditMode;
    LCDManager(Screen *arrayOfScreens, unsigned char numberOfScreens) {
        // Init structures
        screenArray = arrayOfScreens;
        numScreens = numberOfScreens;
        isInEditMode = 0;
    };
    void Init() {
        // Init LCD    
        lcd.begin(16,2);
        cursorInvizibil();
        lcd.clear();
        mutaCursorLaPozitia(0,0);
        nivelIluminare(29);
        currentScreen = 0;

        // Display current screen
        updateScreen();
    }

    char isAtTop() {
        return !isInEditMode;
    };
    char isEditing() {
        return isInEditMode;
    };
    void nextScreen() {
        currentScreen++;
        if (currentScreen == numScreens)
            currentScreen = 0;
        updateScreen();
    };
    void prevScreen() {
        currentScreen--;
        if (currentScreen > numScreens)
            currentScreen = numScreens - 1;
        updateScreen();
    };

    void updateScreen() {
        screenArray[currentScreen].display();
    }

    void enterEditing() {
        isInEditMode = 1;
        screenArray[currentScreen].beginCursor();
    }

    void exitEditing() {
        isInEditMode = 0;
        screenArray[currentScreen].endCursor();
    }

    void select() {
        if(isInEditMode)
            screenArray[currentScreen].select();
    }

    void goUp() {
        if(isInEditMode)
            screenArray[currentScreen].goUp();
    }

    void goDown() {
        if(isInEditMode)
            screenArray[currentScreen].goDown();
    }

    void left() {
        if(isInEditMode)
            screenArray[currentScreen].left();
    }

    void right() {
        if(isInEditMode)
            screenArray[currentScreen].right();
    }
};

class Menu {
    LCDManager *lcdManager;
    
public:
    Menu(LCDManager *manager) {
        lcdManager = manager;
    };

    void moveUp();
    void moveDown();
    void moveLeft();
    void moveRight();
    void select();
    void escape();
};

void  Menu::moveUp() {
    lcdManager->goUp();
}

void  Menu::moveDown() {
    lcdManager->goDown();
}

void  Menu::moveLeft() {
    if (lcdManager->isAtTop())
        lcdManager->prevScreen();
    else 
        lcdManager->left();
}

void  Menu::moveRight() {
    if (lcdManager->isAtTop())
        lcdManager->nextScreen();
    else    
        lcdManager->right();
}

void  Menu::select() {
    if (lcdManager->isAtTop())
        lcdManager->enterEditing();
    else
        lcdManager->select();
}

void  Menu::escape() {
    if(lcdManager->isEditing())
        lcdManager->exitEditing();
}

void initPins() {
// Digital Output Pins

};

// Variables to change and save to EEPROM
void saveDataToEEPROM() {
  EEPROM.write(0,highByte(cod_traseu));
  EEPROM.write(1,lowByte(cod_traseu));
};

// Initialization of menu
// Ultima cifra de pe coloana ne zice ce fel de valori folosim
//  0 - fara zecimale, interval in secunde
//  1 - cu zecimale, interval in sutimi de secunda
MenuItem miTErr1 = MenuItem("Tip panou", NULL, "", 1,  6, 0);
MenuItem miHStiva= MenuItem("Inaltime ", NULL, "", 1, 75, 0);
MenuItem s1[] = {miTErr1, miHStiva};

MenuItem miTErr2 = MenuItem("T ERR M1", NULL, "", 0,255, 0);
MenuItem miTErr3 = MenuItem("T ERR C1", NULL, "", 0, 255, 1);
MenuItem s2[] = {miTErr2, miTErr3};


Screen screen1 = Screen(s1, sizeof(s1)/sizeof(MenuItem));
Screen screen2 = Screen(s2, sizeof(s2)/sizeof(MenuItem));

Screen screens[] = {screen1, screen2};
LCDManager lcdManager = LCDManager(screens, sizeof(screens)/sizeof(Screen));
Menu menu = Menu (&lcdManager);

void setup()
{
   Serial.begin(300);
   Serial.println(F("Starting"));
   lcdManager.Init();
   kpd.init();
 
   pinMode(10, OUTPUT);

   if (!SD.begin(10)) {
     Serial.println(F("SD init failed!"));
     mutaCursorLaPozitia(0, 0);
     lcd.print(CLEAR_LINE);
     mutaCursorLaPozitia(0, 0);
     lcd.print("ERR: SD INIT");
     while(true);;
   } else {
     Serial.println(F("SD init done."));
   }
 
   Serial.println(F("Setup done."));
   
   byte high = EEPROM.read(0);
   byte low = EEPROM.read(1);
   cod_traseu = word(high, low);
}

#define MAX_FILE_NAME 20
char file_name[MAX_FILE_NAME+1] = {0};
File traseuFile;

void loop()
{
  snprintf(file_name, MAX_FILE_NAME, "%d.txt",cod_traseu);
  traseuFile = SD.open(file_name);
  
  char main_line[17] = {0};
  char second_line[17] = {0};
  char this_char = 0;

  mutaCursorLaPozitia(0, 0);
  lcd.print(CLEAR_LINE);
  mutaCursorLaPozitia(0, 0);
  
  if(traseuFile) {
    char cursor = 0;
    Serial.print("\\");
    Serial.print('A');
    Serial.print('A');
    while (traseuFile.available()) {
      this_char = traseuFile.read();
      Serial.write(this_char);
      if(this_char == '\\') {
        mutaCursorLaPozitia(1, 0);
        lcd.print(CLEAR_LINE);
        mutaCursorLaPozitia(1, 2);
      } else {
        if(this_char != '\n')
          lcd.write(this_char);
      }
    }
    Serial.println(); // Just in case there is a missing \n in the file
    // close the file:
    traseuFile.close();
  } else {
    mutaCursorLaPozitia(0, 0);
    lcd.print(CLEAR_LINE);
    mutaCursorLaPozitia(0, 0);
    lcd.print("ERR: SD READ");
    mutaCursorLaPozitia(1, 0);
    lcd.print(CLEAR_LINE);
    mutaCursorLaPozitia(1, 0);
    lcd.print(file_name);
  }
  
  // Lock until we get a number from the kb
  int nr = GetNumber();
  if(nr != 0){
    cod_traseu = nr;    
    saveDataToEEPROM();
  }
}


