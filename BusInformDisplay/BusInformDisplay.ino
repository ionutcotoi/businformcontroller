/*
 BusInformDisplay
 Event Driven RS485 LED Display
 Copyright 2014 - OpenDev Software Development SRL
 Author: Ionut Cotoi
 */
#include <SPI.h>        //SPI.h must be included as DMD is written by SPI (the IDE complains otherwise)
#include <DMD.h>        //
#include <TimerOne.h>   //
#include "SystemFont5x7.h"
#include "Arial_black_16.h"
#include "Arial14.h"
#include "EEPROM.h"

unsigned char DISPLAYS_ACROSS = 2;
unsigned char DISPLAYS_DOWN = 1;
#define       DISPLAY_NUM_LEDS (DISPLAYS_ACROSS*32) 
DMD dmd(DISPLAYS_ACROSS, DISPLAYS_DOWN);

String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete


#define BI_MAX_MESSAGE_LEN 64
#define BI_MAX_LINENR_LEN  3
#define BI_MAX_LINE_LEN    14

char buf[BI_MAX_MESSAGE_LEN];

#define BI_START_CHAR  '\\'
#define BI_STRING_SEP  '\\'
#define BI_LINE_SEP    ' '

#define BI_BIG_TEXT_JUSTIFY     22
#define BI_BIG_TEXT_TOP_JUSTIFY 1
#define BI_SINGLE_BIG_MAX_LINE_LEN 8

char CRC = 0;
char model = 0;
char traseu_str[BI_MAX_LINENR_LEN+1] = {0};

char main_line[BI_MAX_LINE_LEN+1] = {0};
char second_line[BI_MAX_LINE_LEN+1] = {0};

char str_complet[BI_MAX_LINE_LEN*2+3] = {0};

int len_total = 0;
boolean have_second_line = false;
boolean need_redraw = false;
boolean need_rescroll = false;

#define EE_TRASEU_ADDR 0
#define EE_MAIN_L_ADDR (EE_TRASEU_ADDR+10)
#define EE_SECOND_ADDR (EE_MAIN_L_ADDR+BI_MAX_LINENR_LEN+10)
#define EE_HAVE_SECOND_LINE  (EE_SECOND_ADDR+64)

void storeInEE() {
  // Write 0z in the EEPROM area that we use
  for(int i=0; i<EE_SECOND_ADDR+BI_MAX_LINE_LEN+10; i++) {
    EEPROM.write(i, 0);
  }
  
  // Save traseu str
  for(int i=0; i<strlen(traseu_str); i++) {
    EEPROM.write(EE_TRASEU_ADDR+i, traseu_str[i]); 
  }
  
  // Save main line str
  for(int i=0; i<BI_MAX_LINE_LEN; i++) {
    EEPROM.write(EE_MAIN_L_ADDR+i, main_line[i]); 
  }
  
  // Save second line str
  for(int i=0; i<BI_MAX_LINE_LEN; i++) {
    EEPROM.write(EE_SECOND_ADDR+i, second_line[i]); 
  }
  
  EEPROM.write(EE_HAVE_SECOND_LINE, have_second_line);
}

void readFromEE() {
  for(int i=0; i<2; i++) {
    traseu_str[i] = EEPROM.read(EE_TRASEU_ADDR+i); 
  }
  
  for(int i=0; i<BI_MAX_LINE_LEN; i++) {
    main_line[i] = EEPROM.read(EE_MAIN_L_ADDR+i); 
  }
  
  for(int i=0; i<BI_MAX_LINE_LEN; i++) {
    second_line[i] = EEPROM.read(EE_SECOND_ADDR+i); 
  }
  
  have_second_line = EEPROM.read(EE_HAVE_SECOND_LINE);
}

boolean differFromEE() {
  for(int i=0; i<strlen(main_line); i++) {
    if (main_line[i] != EEPROM.read(EE_MAIN_L_ADDR+i))
      return true;
  }
  
  for(int i=0; i<strlen(second_line); i++) {
    if (second_line[i] != EEPROM.read(EE_SECOND_ADDR+i))
      return true;
  }
  
  return false;
}

long last_rescroll;

/*
 Interrupt handler for Timer1 (TimerOne) driven DMD refresh scanning, this gets
 called at the period set in Timer1.initialize();
 */
void ScanDMD() { 
  dmd.scanDisplayBySPI();
}

void scrollText() {
   dmd.drawMarquee(str_complet, strlen(str_complet), (32*DISPLAYS_ACROSS)-1, 2);
   long timer=millis();
   boolean ret=false;
   
   serialEvent();
   
   while(!ret && need_rescroll) {
     
     serialEvent();
   
     if ((timer+50) < millis()) {
       ret = dmd.stepMarquee(-1,0);
       timer = millis();
     } else {
       serialEvent();
     }
   }
   last_rescroll = millis();
}

void setup() {
  //initialize TimerOne's interrupt/CPU usage used to scan and refresh the display
  Timer1.initialize(3000);             // period in microseconds to call ScanDMD. Anything longer than 5000 (5ms) and you can see flicker.
  Timer1.attachInterrupt( ScanDMD );   // attach the Timer1 interrupt to ScanDMD which goes to dmd.scanDisplayBySPI()

  //clear/init the DMD pixels held in RAM
  dmd.clearScreen( true );   //true is normal (all pixels off), false is negative (all pixels on)
  dmd.selectFont(SystemFont5x7);
  
  // initialize serial:
  Serial.begin(300);
  // reserve 200 bytes for the inputString:
  inputString.reserve(BI_MAX_MESSAGE_LEN);
  pinMode(5, OUTPUT);
  digitalWrite(5, LOW);
  
  readFromEE();
  drawDisplay();
}

void loop() {
  // print the string when a newline arrives:
  if (stringComplete) {
    parseMessage();
    // clear the string:
    inputString = "";
    stringComplete = false;
  }
  
  serialEvent();
  if(need_redraw) {
    drawDisplay();
  }
  
  if(last_rescroll + 2000 < millis()) {
    if(need_rescroll) {
      scrollText();
    }
  }
}

void parseMessage() {
  int l = 0;
  unsigned char cursor = 3;
  have_second_line = false;
  memset(traseu_str, 0, sizeof(traseu_str));
  memset(main_line, 0, sizeof(main_line));
  memset(second_line, 0, sizeof(second_line));
  
  inputString.toCharArray(buf, BI_MAX_MESSAGE_LEN) ;
  l=inputString.length();
  
  if(buf[0] == BI_START_CHAR) {
    // Avem caracter de start
    CRC = buf[1];
    model = buf[2];
    
    while((buf[cursor] != BI_LINE_SEP)) {
      traseu_str[cursor-BI_MAX_LINENR_LEN] = buf[cursor];
      cursor++;
    }
    
    unsigned char line_cur = 0;
    if(true) {
      cursor++; // Skip the separator space
      while((buf[cursor] != BI_STRING_SEP) &&
            (buf[cursor] != '\n') &&
            (line_cur < BI_MAX_LINE_LEN)) {
        main_line[line_cur] = buf[cursor];
        cursor ++;
        line_cur ++; 
      }
      if(buf[cursor] == BI_STRING_SEP) {
        have_second_line = true;
        cursor++;
        line_cur = 0;
      }
      
      while(have_second_line && 
            buf[cursor]!='\n' &&
            line_cur < BI_MAX_LINE_LEN) {
        second_line[line_cur] = buf[cursor];
        cursor++;
        line_cur ++;
      }
      
      if(differFromEE()) {
         need_redraw = true;
      }
      need_rescroll = false;
    }
    storeInEE();
  } else {
    
  }
}

void drawDisplay() {
  len_total = 0;
  dmd.clearScreen( true );

  if(DISPLAYS_DOWN == 1) {
    if(DISPLAYS_ACROSS == 1) {
      dmd.selectFont(Arial_Black_16);
      if(strlen(traseu_str) == 1)
        dmd.drawString(12, BI_BIG_TEXT_TOP_JUSTIFY, traseu_str, strlen(traseu_str), GRAPHICS_NORMAL);
      else 
        dmd.drawString(6, BI_BIG_TEXT_TOP_JUSTIFY, traseu_str, strlen(traseu_str), GRAPHICS_NORMAL);
      need_redraw = false;
    } else if(DISPLAYS_ACROSS == 2) {
      // 2 Displays - Scrolling - Pentru Iasi
      unsigned int pixel_len = 0;
      unsigned char hOffset = 0;
      dmd.selectFont(Arial_14);
      
      len_total=strlen(main_line)+strlen(second_line);
      
      if(strlen(second_line)<12) {
        if (have_second_line){
          for(int i=0; i<strlen(second_line); i++)
            pixel_len += dmd.charWidth(second_line[i]);
          pixel_len += strlen(second_line);
          hOffset = (DISPLAY_NUM_LEDS - pixel_len) / 2;
          dmd.drawString(hOffset, 2, second_line, strlen(second_line), GRAPHICS_NORMAL);
        }
        else{
          for(int i=0; i<strlen(main_line); i++)
            pixel_len += dmd.charWidth(main_line[i]);
          pixel_len += strlen(main_line);
          hOffset = (DISPLAY_NUM_LEDS - pixel_len) / 2;
          dmd.drawString(hOffset, 2, main_line, strlen(main_line), GRAPHICS_NORMAL);
        }
        need_redraw = false;
      } else {
         memset(str_complet, 0, sizeof(str_complet));
         strcat(str_complet, main_line);
         strcat(str_complet, "-");
         strcat(str_complet, second_line);
         need_redraw = false;
         scrollText();
         need_rescroll = true;
      }     
       // END 2 Displays
    } else if(DISPLAYS_ACROSS == 3) {
      dmd.selectFont(Arial_Black_16);
      if(strlen(traseu_str) == 1)
        dmd.drawString(5, BI_BIG_TEXT_TOP_JUSTIFY, traseu_str, strlen(traseu_str), GRAPHICS_NORMAL);
      else
        dmd.drawString(0, BI_BIG_TEXT_TOP_JUSTIFY, traseu_str, strlen(traseu_str), GRAPHICS_NORMAL);
     
      if(have_second_line && (strlen(second_line)>1)) {
        dmd.selectFont(SystemFont5x7);
        dmd.drawString(BI_BIG_TEXT_JUSTIFY, 0, main_line, strlen(main_line), GRAPHICS_NORMAL );
        dmd.drawString(BI_BIG_TEXT_JUSTIFY, 8, second_line, strlen(second_line), GRAPHICS_NORMAL );
      } else {
        if(strlen(main_line)<8) {
          dmd.selectFont(Arial_Black_16);
          dmd.drawString(BI_BIG_TEXT_JUSTIFY, BI_BIG_TEXT_TOP_JUSTIFY, main_line, strlen(main_line), GRAPHICS_NORMAL );
        } else {
          dmd.selectFont(SystemFont5x7);
          dmd.drawString(BI_BIG_TEXT_JUSTIFY, 0, main_line, strlen(main_line), GRAPHICS_NORMAL );
        }
      }
      need_redraw = false;
    } else if(DISPLAYS_ACROSS == 4) {
      dmd.selectFont(Arial_Black_16);
      if(strlen(traseu_str) == 1)
        dmd.drawString(5, BI_BIG_TEXT_TOP_JUSTIFY, traseu_str, strlen(traseu_str), GRAPHICS_NORMAL);
      else
        dmd.drawString(0, BI_BIG_TEXT_TOP_JUSTIFY, traseu_str, strlen(traseu_str), GRAPHICS_NORMAL);
      
      dmd.selectFont(SystemFont5x7);
      dmd.drawString(BI_BIG_TEXT_JUSTIFY, 0, main_line, strlen(main_line), GRAPHICS_NORMAL );
      if(have_second_line)
        dmd.drawString(BI_BIG_TEXT_JUSTIFY, 8, second_line, strlen(second_line), GRAPHICS_NORMAL );
      need_redraw = false;
    } 
  }
}

/*
 SerialEvent occurs whenever a new data comes in the
 hardware serial RX.  This routine is run between each
 time loop() runs, so using delay inside loop can delay
 response.  Multiple bytes of data may be available.
 */
void serialEvent() {
  while (Serial.available()) {
    need_rescroll = false;
    // get the new byte:
    char inChar = (char)Serial.read(); 
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    } 
  }
}



