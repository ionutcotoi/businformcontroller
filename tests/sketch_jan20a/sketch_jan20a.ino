#include <SPI.h>        //SPI.h must be included as DMD is written by SPI (the IDE complains otherwise)
#include <DMD.h>        //
#include <TimerOne.h>   //
#include "Arial_black_16.h"

DMD dmd(2,1);

void ScanDMD()
{ 
  dmd.scanDisplayBySPI();
}


void setup() {
  // put your setup code here, to run once:
   //initialize TimerOne's interrupt/CPU usage used to scan and refresh the display
   Timer1.initialize( 4000 );           //period in microseconds to call ScanDMD. Anything longer than 5000 (5ms) and you can see flicker.
   Timer1.attachInterrupt( ScanDMD );   //attach the Timer1 interrupt to ScanDMD which goes to dmd.scanDisplayBySPI()

   //clear/init the DMD pixels held in RAM
   dmd.clearScreen(true);   //true is normal (all pixels off), false is negative (all pixels on)
   dmd.selectFont(Arial_Black_16);
   dmd.drawString(1, 3, "TEST", 4, GRAPHICS_NORMAL ); 
}

void loop() {
  // put your main code here, to run repeatedly:
  
}
