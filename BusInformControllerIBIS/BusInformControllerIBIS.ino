/*
    This example demonstrates basic use of the A-Star 32U4
    as an IBIS controller
    Author: Ionut Cotoi, Gabriel Arnautu
    Latest update: Jan 12, 2017

    Copyright (c) 2017 DEVICEHUB

    Fisierul telegrams.h se genereaza automat folosind: 
    ./gen_tele.py fisier.xls
    din directorul curent
*/

#include <AStar32U4.h>
#include "Keypad.h"
#include "telegrams.h"
#include <EEPROM.h>


#define BROSE_DS3 1
// #define AEG       1
// #define VAN_HOOL     1

#ifdef BROSE_DS3
  #define ONLY_Z_TELEGRAMS 1
  #define L_TELEGRAMS      1
#else

  #ifdef AEG
    #define AEG_STARTUP      1
  #else
    #ifdef VAN_HOOL
      #define L_TELEGRAMS    1
      #define ZA30_TELEGRAMS 1
      #define ZB30_TELEGRAMS 1
    #endif
  #endif  
#endif

// Comment the following line if we want to send other types of telegrams
// #define ONLY_Z_TELEGRAMS 1
// #define L_TELEGRAMS      1
// #define AEG_STARTUP      1

const byte ROWS = 4; //four rows
const byte COLS = 3; //three columns
char keys[ROWS][COLS] = {
  {'1', '2', '3'},
  {'4', '5', '6'},
  {'7', '8', '9'},
  {'*', '0', '#'}
};

// Comenteaza linia urmatoare pentru keypad cu ribbon in partea de jos
// #define TOP_RIBBON  100
// #define BOTTOM_RIBBON  100

#ifdef TOP_RIBBON
byte rowPins[ROWS] = {A3, A4, A5, 4};
byte colPins[COLS] = {A0, A1, A2};
#elif BOTTOM_RIBBON
byte rowPins[ROWS] = {A0, A1, A2, A3};
byte colPins[COLS] = {A4, A5, 4}; // 4 este pinul 1 de la tastatura
// https://cdn-shop.adafruit.com/large/membranekeypad34arduino_LRG.jpg
#else // OptimusDigital 4x4 keyboard
byte rowPins[ROWS] = {4, A5, A4, A3};
byte colPins[COLS] = {A2, 9, A0};
#endif

#define DELAY_BTWN_TELEGRAMS  1200

Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );

AStar32U4LCD lcd;

unsigned int current_tg_code = 101;
unsigned long lastTelegramSentAt = 100000;

template <typename T> void PROGMEM_readAnything (const T * sce, T& dest) {
  memcpy_P (&dest, sce, sizeof (T));
}

template <typename T> T PROGMEM_getAnything (const T * sce) {
  static T temp;
  memcpy_P (&temp, sce, sizeof (T));
  return temp;
}

template< typename T, size_t N > size_t ArraySize (T (&) [N]) {
  return N;
}

// TO TEST IBIS SERIAL
// picocom /dev/tty.usbserial-00001014 --baud 1200 --parity e --databits 7 --stopbits 2

void setup()
{
  // Init serial ports
  Serial.begin(115200);
  Serial.println("BUSINFORM CONTROLLER IBIS");

  Serial1.begin(1200, SERIAL_7E2);

  // Init LCD
  lcd.clear();
  lcd.print("OpenDev");
  lcd.gotoXY(0, 1);
  lcd.print("BInformC");

  delay(2000);
  Serial.print("Size: ");
  Serial.println(ArraySize(telegrame));

  for (int i = 0; i < ArraySize (telegrame); i++) {
    ibisTelegram thisItem;
    PROGMEM_readAnything (&telegrame [i], thisItem);
    Serial.print (i);
    Serial.print (F(" = "));
    Serial.print (thisItem.t_code);
    Serial.print (F(" / "));
    Serial.print (thisItem.traseu_sn);
    Serial.print (F(" / "));
    Serial.print (thisItem.traseu_front_1);
    Serial.println ();
  }  // end of for loop

  delay(8000);

#ifdef AEG_STARTUP
  // Send AEG Special Boot Telegrams
  sendAEGStartup();
#else 
  // Send 0 IBIS telegram
  findAndSendIbisTelegramsForCode(0);
#endif

  delay(DELAY_BTWN_TELEGRAMS);
  // Load the last IBIS code from EEPROM and send it to the wire
  current_tg_code = EEPROM.read(100);
  findAndSendIbisTelegramsForCode(current_tg_code);

  lcd.clear();
  lcd.print("Code ");
  lcd.print(current_tg_code, DEC);
  lcd.gotoXY(0, 1);
}

void sendAEGStartup() {
  ibisTelegram zA5start = {0, "999", "TRANSPORT LOCAL ", "TARGU MURES     ", "TRANSPORT LOCAL ", "TARGU MURES     ", ".HIAIHIAI     "};

  Serial.println("AEG Startup");
  
  sendAEGBootStartup();
  delay(3000);
  sendAEGBootStartup();
  delay(3000);
}

void sendAEGBootStartup() {
  ibisTelegram lE00 = {0, "E00", "TRANSPORT LOCAL ", "TARGU MURES     ", "TRANSPORT LOCAL ", "TARGU MURES     ", ".HIAIHIAI     "};
  ibisTelegram l001 = {0, "001", "TRANSPORT LOCAL ", "TARGU MURES     ", "TRANSPORT LOCAL ", "TARGU MURES     ", ".HIAIHIAI     "};

  Serial.println("AEG Boot Startup");

  sendLineNumberTelegram(lE00);
  sendLineNumberTelegram(l001);
}

void sendLineNumberTelegram(ibisTelegram &thisItem) {
  char send_buff[HEADLINE_LEN + 2];

  // Send line number telegram
  memset(send_buff, 0, sizeof(send_buff));
  int i = 0;
  send_buff[i] = 'l';
  i++;
  for (; i < ROUTE_SHORT_NAME_LEN + 1; i++) {
    send_buff[i] = thisItem.traseu_sn[i - 1];
  }
  send_buff[i - 1] = 0x0D;
  send_buff[i] = 0x7F;
  for (int j = 0; j < i; j++) {
    send_buff[i] ^= send_buff[j];
  }

  // Debug
  Serial.println("HEX TELE");
  for (int j = 0; j < strlen(send_buff); j++) {
    Serial.print("<");
    Serial.print("0x");
    Serial.print(send_buff[j], HEX);
    Serial.print(">");
  }
  Serial.println();

  Serial1.write((char *)send_buff);
}

// See https://github.com/Mezgrman/pyIBIS
void sendExtendedTelegram(ibisTelegram &thisItem) {
  char send_buff[256];

  Serial.println(sizeof(send_buff), DEC);
  // Send line number telegram
  memset(send_buff, 0, sizeof(send_buff));
  snprintf(send_buff,
           sizeof(send_buff),
           "zA5%s%s%s%s\n.%s\r",
           thisItem.traseu_front_1,
           thisItem.traseu_front_2,
           thisItem.traseu_side_1,
           thisItem.traseu_side_2,
           thisItem.traseu_control);

  Serial.println(send_buff);

  int i = strlen(send_buff);

  send_buff[i] = 0x7F;
  for (int j = 0; j < i; j++) {
    send_buff[i] ^= send_buff[j];
  }

  // Debug
  Serial.println("HEX TELE EXTENDED");
  for (int j = 0; j < strlen(send_buff); j++) {
    Serial.print("<");
    Serial.print("0x");
    Serial.print(send_buff[j], HEX);
    Serial.print(">");
  }
  Serial.println();

  Serial1.write((char *)send_buff);
}

void sendZA30UExtendedTelegram(ibisTelegram &thisItem) {
  char send_buff[256];
  memset(send_buff, 0, sizeof(send_buff));
  snprintf(send_buff,
           sizeof(send_buff),
           "zA30U%s\r",
           thisItem.traseu_front_1);

  Serial.println(send_buff);

  int i = strlen(send_buff);

  send_buff[i] = 0x7F;
  for (int j = 0; j < i; j++) {
    send_buff[i] ^= send_buff[j];
  }

  // Debug
  Serial.println("HEX TELE ZA30U");
  for (int j = 0; j < strlen(send_buff); j++) {
    Serial.print("<");
    Serial.print("0x");
    Serial.print(send_buff[j], HEX);
    Serial.print(">");
  }
  Serial.println();

  Serial1.write((char *)send_buff);
}

void sendZB30UExtendedTelegram(ibisTelegram &thisItem) {
  char send_buff[256];
  memset(send_buff, 0, sizeof(send_buff));
  snprintf(send_buff,
           sizeof(send_buff),
           "zB30U%s\r",
           thisItem.traseu_side_1);

  Serial.println(send_buff);

  int i = strlen(send_buff);

  send_buff[i] = 0x7F;
  for (int j = 0; j < i; j++) {
    send_buff[i] ^= send_buff[j];
  }

  // Debug
  Serial.println("HEX TELE ZB30U");
  for (int j = 0; j < strlen(send_buff); j++) {
    Serial.print("<");
    Serial.print("0x");
    Serial.print(send_buff[j], HEX);
    Serial.print(">");
  }
  Serial.println();

  Serial1.write((char *)send_buff);
}

void sendIbisTelegram(unsigned char idx) {
  ibisTelegram thisItem;
  PROGMEM_readAnything(&telegrame[idx], thisItem);

  sendLineNumberTelegram(thisItem);
  delay(DELAY_BTWN_TELEGRAMS);
  sendExtendedTelegram(thisItem);
}

void sendZIbisTelegram(unsigned int idx) {
  char send_buff[Z_LEN + 2];

  // Send line number telegram
  memset(send_buff, 0, sizeof(send_buff));
  int i = 0;
  send_buff[i] = 'z';
  i++;

  if (idx < 10) {
    send_buff[i] = '0';
    i ++;
    send_buff[i] = '0';
    i ++;
    send_buff[i] = idx + 48;
    i++;
  } else if (idx < 100) {
    send_buff[i] = '0';
    i ++;
    unsigned int fc = idx / 10;
    send_buff[i] = fc + 48;
    i++;
    send_buff[i] = idx - fc * 10 + 48;
    i++;
  } else {
    unsigned int fc = idx / 100;
    send_buff[i] = fc + 48;
    i++;
    unsigned int fd = idx / 10 % 10;
    send_buff[i] = fd + 48;
    i++;
    send_buff[i] = idx - fd * 10 - fc * 100 + 48;
    i++;
  }
  send_buff[i] = '\r';
  i++;
  send_buff[i] = 0x7F;

  for (int j = 0; j < i; j++) {
    send_buff[i] ^= send_buff[j];
  }

  // Debug
  Serial.println("Z HEX TELE");
  for (int j = 0; j < strlen(send_buff); j++) {
    Serial.print("<");
    Serial.print("0x");
    Serial.print(send_buff[j], HEX);
    Serial.print(">");
  }
  Serial.println();

  Serial1.write((char *)send_buff);
}


void findAndSendIbisTelegramsForCode(unsigned int code) {
  lcd.clear();
  lcd.print(" Update");
  lcd.gotoXY(0, 1);
  lcd.print(" ");
  lcd.print(code);

#ifdef EXTENDED_TELEGRAMS
  for (int i = 0; i < ArraySize (telegrame); i++) {
    ibisTelegram thisItem;
    PROGMEM_readAnything (&telegrame[i], thisItem);
    if (thisItem.t_code == code) {
      Serial.print("Found IBIS telegram at index: ");
      Serial.println(i);
      sendIbisTelegram(i);
    }
  }
#endif
  
#ifdef ONLY_Z_TELEGRAMS
  sendZIbisTelegram(code);
#endif /* ONLY_Z_TELEGRAMS */

#ifdef L_TELEGRAMS
  bool found = false;
  ibisTelegram thisItem;
  
  for (int i = 0; i < ArraySize (telegrame); i++) {
    PROGMEM_readAnything (&telegrame[i], thisItem);
    if (thisItem.t_code == code) {
      found = true;
      sendLineNumberTelegram(thisItem);
    }
  }

  if (found == false) {
    PROGMEM_readAnything (&telegrame[0], thisItem);
    sendLineNumberTelegram(thisItem);
  }
#endif

#ifdef ZA30_TELEGRAMS
  delay(DELAY_BTWN_TELEGRAMS);
  for (int i = 0; i < ArraySize (telegrame); i++) {
  ibisTelegram thisItem;
  PROGMEM_readAnything (&telegrame[i], thisItem);
  if (thisItem.t_code == code) {
      Serial.print("Found IBIS telegram at index: ");
      Serial.println(i);
      sendZA30UExtendedTelegram(thisItem);
    }
  }
#endif

#ifdef ZB30_TELEGRAMS
  delay(DELAY_BTWN_TELEGRAMS);
  for (int i = 0; i < ArraySize (telegrame); i++) {
  ibisTelegram thisItem;
  PROGMEM_readAnything (&telegrame[i], thisItem);
  if (thisItem.t_code == code) {
      Serial.print("Found IBIS telegram at index: ");
      Serial.println(i);
      sendZB30UExtendedTelegram(thisItem);
    }
  }
#endif

  lastTelegramSentAt = millis();
}

bool isCodeValid(unsigned int code) {
#ifndef ONLY_Z_TELEGRAMS
  for (int i = 0; i < ArraySize (telegrame); i++) {
    ibisTelegram thisItem;
    PROGMEM_readAnything (&telegrame[i], thisItem);
    if (thisItem.t_code == code) {
      return true;
    }
  }
  return false;
#else
  return true;
#endif
}

unsigned int readKeyPadCode() {
  char customKey = keypad.getKey();
  if (customKey) {
    lcd.gotoXY(0, 1);
    lcd.print(customKey);
  }
  return 0;
}

void loop()
{
  int read_code = 101;
  unsigned int l_cursor = 0;
  char key = '\0';
  char key_code_buffer[5] = {0};

  while (true) {

    while (key = keypad.getKey()) {
      if (key == '#') {
        Serial.println(key_code_buffer);
        read_code = atoi(key_code_buffer);
        Serial.print("Read code: ");
        Serial.println(read_code);
        l_cursor = 0;
        memset(key_code_buffer, 0, sizeof(key_code_buffer));
        lcd.clear();
        lcd.print(" Code: ");
        lcd.gotoXY(0, 1);
        lcd.gotoXY(l_cursor, 1);
        if (isCodeValid(read_code)) {
          current_tg_code = read_code;
          findAndSendIbisTelegramsForCode(current_tg_code);

          // Save IBIS telegram code to EEPROM
          EEPROM.write(100, current_tg_code);

          lcd.clear();
          lcd.print("Code ");
          lcd.print(current_tg_code);
          lcd.gotoXY(0, 1);
          lcd.gotoXY(l_cursor, 1);
          lcd.print(key_code_buffer);
        } else {
          lcd.gotoXY(0, 1);
          lcd.print("INVALID");
        }
      } else if (key == '*') {
        l_cursor = 0;
        memset(key_code_buffer, 0, sizeof(key_code_buffer));
        lcd.clear();
        lcd.print(" Code: ");
        lcd.gotoXY(0, 1);
        lcd.gotoXY(l_cursor, 1);
      } else {
        lcd.print(key);
        key_code_buffer[l_cursor] = key;
        l_cursor++;
      }
    }

    // Serial.println(abs(millis() - lastTelegramSentAt));
    // Do update IBIS Telegrams
    if (abs(millis() - lastTelegramSentAt) > DELAY_BETWEEN_TELEGRAMS) {
      findAndSendIbisTelegramsForCode(current_tg_code);
      lcd.clear();
      lcd.print("Code ");
      lcd.print(current_tg_code);
      lcd.gotoXY(0, 1);
    }
  }
}

