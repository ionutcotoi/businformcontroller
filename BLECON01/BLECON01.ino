#include <CurieBLE.h>
#include <EEPROM.h>
#include <SPI.h>
#include "DMD2.h"
#include "fonts/My32x15Font.h"
#include "fonts/SystemFont5x7.h"
#include "fonts/Arial10.h"
#include "fonts/Arial12.h"
#include "fonts/Arial14.h"
#include "fonts/Arial16.h"
#include "fonts/Arial_Black_16.h"
#include "fonts/Droid_Sans_24.h"
#include "fonts/Droid_Sans_16.h"
#include "fonts/OpenDev_32_Big.h"
// FONTS: https://github.com/SmallRoomLabs/QuarterK/tree/master/library

SPIDMD* dmd = NULL;  // DMD controls the entire display
DMD_TextBox *box = NULL;

BLEPeripheral blePeripheral;       // BLE Peripheral Device (the board you're programming)
BLEService    serialService("713d0000-503e-4c75-ba94-3148f18d941e"); // BLE LED Communication Service

// BLE text characteristics
BLECharacteristic a_traseu             ("713d0003-503e-4c75-ba94-3148f18d941e", BLERead | BLEWrite, 4 );
BLECharacteristic a_main               ("713d0003-503e-4c75-ba94-3148f18d942e", BLERead | BLEWrite, 20);
BLECharacteristic a_second             ("713d0003-503e-4c75-ba94-3148f18d943e", BLERead | BLEWrite, 20);

BLEIntCharacteristic sw_version        ("713d0003-503e-4c75-ba94-3148f18d945e", BLERead | BLENotify);

// BLE message checksum characteristic
BLEUnsignedIntCharacteristic checksum  ("713d0003-503e-4c75-ba94-3148f18d946e", BLERead | BLENotify);

// BLE informative characteristics
BLECharacteristic             mac      ("713d0003-503e-4c75-ba94-3148f18d947e", BLERead, 6);
BLEUnsignedCharCharacteristic bright   ("713d0003-503e-4c75-ba94-3148f18d948e", BLERead | BLEWrite);
BLEUnsignedCharCharacteristic info     ("713d0003-503e-4c75-ba94-3148f18d949e", BLERead);

// Only allow connections from these BLE addresses:
// Mihai Karbon devel      - d8:3c:69:00:ce:73
// Ionut HTC M7 devel      - 53:bb:b1:53:34:40
// Zalau LG production     - 
// Zalau Philips 1 prod    -
// Zalau Philips 2 prod    -
// Zalau Philips 3 prod    -
// Iasi  Karbon production - d8:3c:69:00:e9:c2

byte numPanels = 1;
byte numRows = 1;
bool isInterior = false;
boolean have_second_line = false;
boolean need_redraw = false;
boolean need_rescroll = false;

#define BI_MAX_MESSAGE_LEN 64
#define BI_MAX_LINENR_LEN   3
#define BI_MAX_LINE_LEN    20

char buf[BI_MAX_MESSAGE_LEN];

#define BI_START_CHAR  '\\'
#define BI_STRING_SEP  '\\'
#define BI_LINE_SEP    ' '

#define BI_BIG_TEXT_JUSTIFY       22
#define BI_BIG_TEXT_TOP_JUSTIFY    1
#define BI_SINGLE_BIG_MAX_LINE_LEN 8

char CRC = 0;
char model = 0;

char traseu_str [BI_MAX_LINENR_LEN + 1] = {0};
char main_line  [BI_MAX_LINE_LEN   + 1] = {0};
char second_line[BI_MAX_LINE_LEN   + 1] = {0};

char str_complet[BI_MAX_LINE_LEN * 2 + 10] = {0};
int len_total = 0;

unsigned long connectedAt = 0;
unsigned long updatedAt = 0;
unsigned long aliveAt = 0;

const char *next;
const char *str_start;

int eeAddress = 0;
const unsigned int val_sw_version = 8;

struct EE_Data {
  char traseu_str [BI_MAX_LINENR_LEN + 1];
  char main_line  [BI_MAX_LINE_LEN   + 1];
  char second_line[BI_MAX_LINE_LEN   + 1];
  boolean have_second_line;
  char brightness;
};

EE_Data myData;

// Software invert
bool inverted = false;

DMDGraphicsMode G_mode = GRAPHICS_ON;

int oldDataChecksum = 0;
unsigned char brightness = 255;

char display_string[BI_MAX_MESSAGE_LEN] = {0};
bool finished_reading_string = false;

void storeInEE() {
  memset(myData.traseu_str, 0, sizeof(myData.traseu_str));
  memset(myData.main_line, 0, sizeof(myData.main_line));
  memset(myData.second_line, 0, sizeof(myData.second_line));
  strncpy(myData.traseu_str, traseu_str, BI_MAX_LINENR_LEN + 1);
  strncpy(myData.main_line, main_line, BI_MAX_LINE_LEN + 1);
  strncpy(myData.second_line, second_line, BI_MAX_LINE_LEN + 1);
  Serial.println(F("Verify data before writing to EEPROM"));
  Serial.print(F("V TRASEU: "));
  Serial.println(myData.traseu_str);
  Serial.print(F("V PRIMAR: "));
  Serial.println(myData.main_line);
  Serial.print(F("V SECUND: "));
  Serial.println(myData.second_line);
  myData.have_second_line = have_second_line;
  myData.brightness = brightness;
  EEPROM.put(eeAddress, myData);
  Serial.println(F("Saved in EEPROM"));
}

void readFromEE() {
  EEPROM.get(eeAddress, myData);
  strncpy(traseu_str, myData.traseu_str, BI_MAX_LINENR_LEN + 1);
  strncpy(main_line, myData.main_line, BI_MAX_LINE_LEN + 1);
  strncpy(second_line, myData.second_line, BI_MAX_LINE_LEN + 1);
  have_second_line = myData.have_second_line;
  brightness = myData.brightness;
  Serial.println(F("Loaded from EEPROM"));
  Serial.print(F("TRASEU: "));
  Serial.println(traseu_str);
  Serial.print(F("PRIMAR: "));
  Serial.println(main_line);
  Serial.print(F("SECUND: "));
  Serial.println(second_line);
}

void verifyReadFromEE() {
  EEPROM.get(eeAddress, myData);
  have_second_line = myData.have_second_line;
  Serial.println(F("Verify data from EEPROM"));
  Serial.print(F("V TRASEU: "));
  Serial.println(myData.traseu_str);
  Serial.print(F("V PRIMAR: "));
  Serial.println(myData.main_line);
  Serial.print(F("V SECUND: "));
  Serial.println(myData.second_line);
}

void setup() {
  char localName[20] = {0};
  delay(5000);
  Serial.begin(57600);
  pinMode(A0, INPUT);
  pinMode(A1, INPUT);
  pinMode(A2, INPUT);
  digitalWrite(A0, HIGH);
  digitalWrite(A1, HIGH);
  digitalWrite(A2, HIGH);

  byte a0 = !digitalRead(A0);
  byte a1 = !digitalRead(A1);
  byte a2 = !digitalRead(A2);

  numPanels += a0 * 1;
  numPanels += a1 * 2;

  numRows += a2 * 1;

  Serial.print(F("NO PANELS: "));
  Serial.println(numPanels);

  Serial.print(F("NO ROWS: "));
  Serial.println(numRows);

  sprintf(localName, "LED%02d%d%d%d", val_sw_version, isInterior, numRows, numPanels);
  
  blePeripheral.setLocalName(localName);
  blePeripheral.setAdvertisedServiceUuid(serialService.uuid());  // add the service UUID
  blePeripheral.addAttribute(serialService);   // Add the BLE Battery service
  blePeripheral.addAttribute(a_traseu); // add traseu characteristic
  blePeripheral.addAttribute(a_main); // add main characteristic
  blePeripheral.addAttribute(a_second); // add second info characteristic
  blePeripheral.addAttribute(sw_version);
  blePeripheral.addAttribute(checksum); // add info characteristic
  blePeripheral.addAttribute(mac); // add info characteristic
  blePeripheral.addAttribute(bright); // add bright characteristic
  blePeripheral.addAttribute(info); // add info characteristic

  info.setValue(numPanels);
  bright.setValue(brightness);
  sw_version.setValue(val_sw_version);

  a_traseu.setEventHandler(BLEWritten, traseuCharacteristicWritten);
  a_main.setEventHandler(BLEWritten, mainCharacteristicWritten);
  a_second.setEventHandler(BLEWritten, secondCharacteristicWritten);

  bright.setEventHandler(BLEWritten, brightnessCharacteristicWritten);

  blePeripheral.begin();
  Serial.println(F("Bluetooth device active, waiting for connections..."));
  delay(1000);
  readFromEE();
  delay(1000);

  bright.setValue(brightness);

  dmd = new SPIDMD(numPanels, numRows);

  if (inverted)
    dmd->setBrightness(255 - brightness);
  else
    dmd->setBrightness(brightness);

  dmd->selectFont(Arial14);
  dmd->begin();

  if (inverted) {
    G_mode = GRAPHICS_INVERSE;
    dmd->drawFilledBox(0, 0, 16, 50, GRAPHICS_ON);
  }
  
  drawDisplay();
  updateCheckSum();
}

void loop() {
  // listen for BLE peripherals to connect:
  BLECentral central = blePeripheral.central();

  // if a central is connected to peripheral:
  if (central) {
    connectedAt = millis();
    Serial.print("Connected to central: ");
    // print the central's MAC address:
    Serial.print(central.address());
    Serial.print(" at time ");
    Serial.println(millis());

    // check the battery level every 200ms
    // as long as the central is still connected:
    while (central.connected()) {
      delay(1000);
      // Disconnect from phone after 15 seconds
      unsigned long now_t = millis();
      Serial.print("aici - ");
      Serial.println(now_t);
      if ((connectedAt + 15000) < now_t) {
        Serial.println("Disconnected due to timeout");
        central.disconnect();
      }

      if ((updatedAt + 1000) < now_t) {
        if (need_redraw) {
          storeInEE();
          verifyReadFromEE();
          drawDisplay();
          updateCheckSum();
        }
      }
      ScrollText();
    }
    Serial.print("Disconnected from central: ");
    Serial.println(central.address());
  }

  unsigned long now_t = millis();
  if ((updatedAt + 1000) < now_t) {
    if (need_redraw) {
      storeInEE();
      verifyReadFromEE();
      drawDisplay();
      updateCheckSum();
    }
  }
  ScrollText();
  if((aliveAt + 2000) < now_t) {
    Serial.println("alive!");
    aliveAt = now_t;
  }
}

void traseuCharacteristicWritten(BLECentral& central, BLECharacteristic& characteristic) {
  have_second_line = false;
  need_rescroll = false;

  Serial.print("Characteristic event - traseu, written: ");
  unsigned char pktLen = a_traseu.valueLength();
  Serial.println(pktLen);

  memset(traseu_str, 0, sizeof(traseu_str));
  strncpy(traseu_str, (char *)a_traseu.value(), pktLen);

  Serial.println(traseu_str);

  need_redraw = true;
  updatedAt = millis();
}

void mainCharacteristicWritten(BLECentral& central, BLECharacteristic& characteristic) {
  Serial.print("Characteristic event - main, written: ");
  unsigned char pktLen = a_main.valueLength();
  Serial.println(pktLen);

  memset(main_line, 0, sizeof(main_line));
  strncpy(main_line, (char *)a_main.value(), pktLen);
  Serial.println(main_line);

  need_redraw = true;
  updatedAt = millis();
}

void secondCharacteristicWritten(BLECentral& central, BLECharacteristic& characteristic) {
  have_second_line = false;
  Serial.print("Characteristic event - second, written: ");
  unsigned char pktLen = a_second.valueLength();
  Serial.println(pktLen);

  memset(second_line, 0, sizeof(second_line));
  strncpy(second_line, (char *)a_second.value(), pktLen);
  Serial.println(second_line);

  if (strlen(second_line) > 1) {
    have_second_line = true;
  }
  need_redraw = true;
  updatedAt = millis();
}

void brightnessCharacteristicWritten(BLECentral& central, BLECharacteristic& characteristic) {
  brightness = bright.value();
  
  if (inverted)
    dmd->setBrightness(255 - brightness);
  else
    dmd->setBrightness(brightness);

  storeInEE();
  bright.setValue(brightness);
}

int getCheckSum() {
  int CRC = 0;

  for (byte x = 0; x<strlen(traseu_str); x++){
      CRC = CRC ^ traseu_str[x];
  }

  for (byte x = 0; x<strlen(main_line); x++){
      CRC = CRC ^ main_line[x];
  } 

  for (byte x = 0; x<strlen(second_line); x++){
      CRC = CRC ^ second_line[x];
  } 

  return CRC;
}

void updateCheckSum() {
  int CRC = getCheckSum();

  if (CRC != oldDataChecksum) {
    Serial.print("Data Checksum is now: ");
    Serial.println(CRC);
    checksum.setValue(CRC);
    oldDataChecksum = CRC;
  }
}

void drawDisplay() {
  unsigned int str_width = 0;
  unsigned int str_align = 0;
  unsigned int str_space = 0;

  unsigned int text_align;

  unsigned int text_str_width;
  unsigned int text_str_align;
  unsigned int text_top_align = 0;
      
  Serial.print("Draw display ");
  Serial.println(numPanels);

  Serial.print("Draw display ");
  Serial.println(numPanels);
  Serial.print("TRASEU: ");
  Serial.print(strlen(traseu_str));
  Serial.print(" ");
  Serial.println(traseu_str);
  Serial.print("MAIN: ");
  Serial.print(strlen(main_line));
  Serial.print(" ");
  Serial.println(main_line);
  Serial.print("SECOND: ");
  Serial.print(strlen(second_line));
  Serial.print(" ");
  Serial.println(second_line);

  dmd->clearScreen();

  if (inverted) {
    dmd->drawFilledBox(0, 0, numPanels * 32, numRows * 16, GRAPHICS_ON);
  }

  if (numRows == 1) {
    if (numPanels == 1) {
      dmd->selectFont(Arial_Black_16);
      str_width = dmd->stringWidth(traseu_str);
      str_align = (32 - str_width) / 2;
      Serial.print("WIDTH: ");
      Serial.println(str_width, DEC);
      Serial.print("ALIGN: ");
      Serial.println(str_align, DEC);
      dmd->drawString(str_align, BI_BIG_TEXT_TOP_JUSTIFY, traseu_str, G_mode);
      need_redraw = false;
    } else if (numPanels == 2) {
      if (box == NULL) {
        box = new DMD_TextBox(*dmd, 0, 2, 2 * 32, 16);
      }
      // 2 Displays - Scrolling - Pentru Iasi
      unsigned int pixel_len = 0;
      int hOffset = 0;
      dmd->selectFont(Arial14);

      len_total = strlen(main_line) + strlen(second_line) + 3;

      if (strlen(second_line) < 12) {
        Serial.println("FARA SCROLL");
        if (have_second_line) {
          if (strlen(second_line) > 7)
            dmd->selectFont(Arial14);
          for (unsigned int i = 0; i < strlen(second_line); i++)
            pixel_len += dmd->charWidth(second_line[i]);
          pixel_len += strlen(second_line);
          hOffset = (numPanels * 32 - pixel_len) / 2;
          if (hOffset > 12)
            hOffset = 0;
          dmd->drawString(hOffset, 2, second_line);
        } else {
          Serial.println("1 linie");
          if (strlen(main_line) > 7)
            dmd->selectFont(Arial14);
          for (unsigned int i = 0; i < strlen(main_line); i++)
            pixel_len += dmd->charWidth(main_line[i]);
          pixel_len += strlen(main_line);
          hOffset = (numPanels * 32 - pixel_len) / 2;
          if (hOffset > 10)
            hOffset = 0;
          dmd->drawString(hOffset, 2, main_line);
        }
        need_redraw = false;
      } else {
        Serial.println("CU SCROLL");
        memset(str_complet, 0, sizeof(str_complet));
        strcat(str_complet, main_line);
        strcat(str_complet, " - ");
        strcat(str_complet, second_line);
        strcat(str_complet, "   ");
        need_redraw = true;
        // TODO scrollText();
        need_rescroll = true;
        next = str_complet;
        need_redraw = false;
      }
      // END 2 Displays
    } else if (numPanels >= 3) {
      dmd->selectFont(Arial_Black_16);
      str_width = dmd->stringWidth(traseu_str) + 2;
      str_space = str_width + 2;
      str_align = (str_space - str_width) / 2;

      text_align = str_width + str_align * 2;
        
      dmd->drawString(str_align, BI_BIG_TEXT_TOP_JUSTIFY, traseu_str, G_mode);
              
      if (have_second_line && (strlen(second_line) > 1)) {
          dmd->selectFont(SystemFont5x7);
          text_str_width = dmd->stringWidth(main_line);
          text_str_align = ((3 * 32) - text_str_width - text_align) / 2 + text_align;
          if (text_str_align > 100) {
            text_str_align = text_align;
          }
          dmd->drawString(text_str_align, 0, main_line, G_mode);
          dmd->drawString(text_str_align, 8, second_line, G_mode);
      } else {
        if (strlen(main_line) < 7) {
          dmd->selectFont(Arial_Black_16);
          text_str_width = dmd->stringWidth(main_line);
          text_str_align = ((3 * 32) - text_str_width - text_align) / 2 + text_align;
          Serial.print("1 1 1 WIDTH: ");
          Serial.println(text_str_width, DEC);
          Serial.print("1 1 1 ALIGN: ");
          Serial.println(text_str_align, DEC);
          dmd->drawString(text_str_align, BI_BIG_TEXT_TOP_JUSTIFY, main_line, G_mode);
        } else {
          dmd->selectFont(Arial14);
          text_str_width = dmd->stringWidth(main_line);
          text_str_align = ((3 * 32) - text_str_width - text_align) / 2 + text_align;
          Serial.print("1 1 2 WIDTH: ");
          Serial.println(text_str_width, DEC);
          Serial.print("1 1 2 ALIGN: ");
          Serial.println(text_str_align, DEC);
          dmd->drawString(text_str_align, BI_BIG_TEXT_TOP_JUSTIFY+1, main_line, G_mode);
        }
      }
      need_redraw = false;
    } /* else if (numPanels == 4) {
      dmd->selectFont(Arial_Black_16);
      if (strlen(traseu_str) == 1)
        dmd->drawString(5, BI_BIG_TEXT_TOP_JUSTIFY, traseu_str, G_mode);
      else
        dmd->drawString(0, BI_BIG_TEXT_TOP_JUSTIFY, traseu_str, G_mode);

      dmd->selectFont(SystemFont5x7);
      dmd->drawString(BI_BIG_TEXT_JUSTIFY, 0, main_line, G_mode);
      if (have_second_line)
        dmd->drawString(BI_BIG_TEXT_JUSTIFY, 8, second_line, G_mode);
      need_redraw = false;
    } */
  } else if (numRows == 2) {
    if (numPanels == 4) {
      if(strlen(traseu_str) == 3) {
        dmd->selectFont(Arial_Black_16);
        str_width = dmd->stringWidth(traseu_str);
        str_space = str_width + 2;
        str_align = (str_space - str_width) / 2;
        Serial.print("T WIDTH: ");
        Serial.println(str_width, DEC);
        Serial.print("T ALIGN: ");
        Serial.println(str_align, DEC);
        dmd->drawString(str_align, 9, traseu_str, G_mode);
      } else {
        dmd->selectFont(My32x15Font);
        str_width = dmd->stringWidth(traseu_str);
        str_space = str_width + 2;
        str_align = (str_space - str_width) / 2;
        Serial.print("T WIDTH: ");
        Serial.println(str_width, DEC);
        Serial.print("T ALIGN: ");
        Serial.println(str_align, DEC);
        dmd->drawString(str_align, 0, traseu_str, G_mode);
      }
      
      unsigned int text_align = str_width + str_align * 2;
      Serial.print("TEXT ALIGN: ");
      Serial.println(text_align, DEC);

      if (str_width == 0) {
        text_align = 0;
      }

      if (have_second_line) {
        dmd->selectFont(Arial_Black_16);

        // First line
        text_str_width = dmd->stringWidth(main_line);
        text_str_align = ((4 * 32) - text_str_width - text_align) / 2 + text_align;
        if (text_str_align > 100) {
          text_str_align = text_align;
        }
        Serial.print("1 WIDTH: ");
        Serial.println(text_str_width, DEC);
        Serial.print("1 ALIGN: ");
        Serial.println(text_str_align, DEC);

        if ((text_str_width + text_str_align) > (numPanels * 32)) {
          // Downgrade font to Arial16
          dmd->selectFont(Arial_16);
          text_str_width = dmd->stringWidth(main_line);
          text_str_align = ((4 * 32) - text_str_width - text_align) / 2 + text_align;
          if (text_str_align > 100) {
            text_str_align = text_align;
          }
          Serial.print("1 WIDTH: ");
          Serial.println(text_str_width, DEC);
          Serial.print("1 ALIGN: ");
          Serial.println(text_str_align, DEC);
        }

        if ((text_str_width + text_str_align) > (numPanels * 32)) {
          // Downgrade font Arial14
          text_top_align = 1;
          dmd->selectFont(Arial14);
          text_str_width = dmd->stringWidth(main_line);
          text_str_align = ((4 * 32) - text_str_width - text_align) / 2 + text_align;
          if (text_str_align > 100) {
            text_str_align = text_align;
          }
          Serial.print("1 WIDTH: ");
          Serial.println(text_str_width, DEC);
          Serial.print("1 ALIGN: ");
          Serial.println(text_str_align, DEC);
        }

        if ((text_str_width + text_str_align) > (numPanels * 32)) {
          // Downgrade font Arial12
          text_top_align = 1;
          dmd->selectFont(Arial_12);
          text_str_width = dmd->stringWidth(main_line);
          text_str_align = ((4 * 32) - text_str_width - text_align) / 2 + text_align;
          if (text_str_align > 100) {
            text_str_align = text_align;
          }
          Serial.print("2 1 WIDTH: ");
          Serial.println(text_str_width, DEC);
          Serial.print("2 1 ALIGN: ");
          Serial.println(text_str_align, DEC);
        }

        if ((text_str_width + text_str_align) > (numPanels * 32)) {
          // Downgrade font Arial10
          text_top_align = 3;
          dmd->selectFont(SystemFont5x7);
          text_str_width = dmd->stringWidth(main_line);
          text_str_align = ((4 * 32) - text_str_width - text_align) / 2 + text_align;
          if (text_str_align > 100) {
            text_str_align = text_align;
          }
          Serial.print("2 1 WIDTH: ");
          Serial.println(text_str_width, DEC);
          Serial.print("2 1 ALIGN: ");
          Serial.println(text_str_align, DEC);
        }

        dmd->drawString(text_str_align, text_top_align, main_line, G_mode);

        // Second line
        dmd->selectFont(Arial_Black_16);
        text_top_align = 16;

        text_str_width = dmd->stringWidth(second_line);
        text_str_align = ((4 * 32) - text_str_width - text_align) / 2 + text_align;
        if (text_str_align > 100) {
          text_str_align = text_align;
        }

        if ((text_str_width + text_str_align) > (4 * 32)) {
          // Downgrade font to Arial16
          dmd->selectFont(Arial_16);
          text_str_width = dmd->stringWidth(main_line);
          text_str_align = ((4 * 32) - text_str_width - text_align) / 2 + text_align;
          if (text_str_align > 100) {
            text_str_align = text_align;
          }
          Serial.print("2 WIDTH: ");
          Serial.println(text_str_width, DEC);
          Serial.print("2 ALIGN: ");
          Serial.println(text_str_align, DEC);
        }

        if ((text_str_width + text_str_align) > (4 * 32)) {
          // Downgrade font Arial14
          text_top_align = 17;
          dmd->selectFont(Arial14);
          text_str_width = dmd->stringWidth(main_line);
          text_str_align = ((4 * 32) - text_str_width - text_align) / 2 + text_align;
          if (text_str_align > 100) {
            text_str_align = text_align;
          }
          Serial.print("2 WIDTH: ");
          Serial.println(text_str_width, DEC);
          Serial.print("2 ALIGN: ");
          Serial.println(text_str_align, DEC);
        }

        if ((text_str_width + text_str_align) > (4 * 32)) {
          // Downgrade font Arial12
          text_top_align = 17;
          dmd->selectFont(Arial_12);
          text_str_width = dmd->stringWidth(main_line);
          text_str_align = ((4 * 32) - text_str_width - text_align) / 2 + text_align;
          if (text_str_align > 100) {
            text_str_align = text_align;
          }
          Serial.print("2 WIDTH: ");
          Serial.println(text_str_width, DEC);
          Serial.print("2 ALIGN: ");
          Serial.println(text_str_align, DEC);
        }

        if ((text_str_width + text_str_align) > (4 * 32)) {
          // Downgrade font Arial10
          text_top_align = 18;
          dmd->selectFont(SystemFont5x7);
          text_str_width = dmd->stringWidth(main_line);
          text_str_align = ((4 * 32) - text_str_width - text_align) / 2 + text_align;
          if (text_str_align > 100) {
            text_str_align = text_align;
          }
          Serial.print("2 WIDTH: ");
          Serial.println(text_str_width, DEC);
          Serial.print("2 ALIGN: ");
          Serial.println(text_str_align, DEC);
        }

        dmd->drawString(text_str_align, text_top_align, second_line, G_mode);
      } else { // Only one line
        unsigned int text_str_width;
        unsigned int text_str_align;
        unsigned int text_top_align = 0;
        dmd->selectFont(My32x15Font);

        text_str_width = dmd->stringWidth(main_line);
        text_str_align = ((numPanels * 32) - text_str_width - text_align) / 2 + text_align;

        if((text_str_width + text_str_align) > numPanels * 32) {
          // Downgrade font to Droid Sans 24
          text_top_align = 4;
          dmd->selectFont(Droid_Sans_24);
          text_str_width = dmd->stringWidth(main_line);
          text_str_align = ((numPanels * 32) - text_str_width - text_align) / 2 + text_align;

          Serial.print(F("1 1 WIDTH: "));
          Serial.println(text_str_width, DEC);
          Serial.print(F("1 1 ALIGN: "));
          Serial.println(text_str_align, DEC);
        }

        if ((text_str_width + text_str_align) > (numPanels * 32)) {
          // Downgrade font to Arial Black 16
          dmd->selectFont(Arial_Black_16);
          text_str_width = dmd->stringWidth(main_line);
          text_str_align = ((numPanels * 32) - text_str_width - text_align) / 2 + text_align;
          
          if (text_str_align > 100) {
            text_str_align = text_align;
          }

          text_top_align = 8;
          
          Serial.print("1 1 WIDTH: ");
          Serial.println(text_str_width, DEC);
          Serial.print("1 1 ALIGN: ");
          Serial.println(text_str_align, DEC);  
        }
        
        dmd->drawString(text_str_align, text_top_align, main_line, G_mode);
      }

      need_redraw = false;
    }
  }
}
unsigned long last_scroll = 0;

void ScrollText() {
  if (need_rescroll) {
    if (last_scroll + 250 < millis()) {
      box->print(*next);
      last_scroll = millis();
      next++;
      if (*next == '\0') {
        next = str_complet;
      }
    }
  }
}


